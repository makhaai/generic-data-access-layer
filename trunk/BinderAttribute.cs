﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Generic.Data
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class IgnoreOnUpdate : System.Attribute
    {
    }
}
