﻿/*
 * SQL Provider
 * Author: Raju Melveetilpurayil
 * Date  : 28-11-2014
 * 
 * */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Generic.Data
{
    /// <summary>
    /// SQL Connection String provider
    /// </summary>
    internal class Provider
    {
        public static SqlConnection GetConnection()
        {
            SqlConnection sqlConnection = null;
            if (ConfigurationManager.ConnectionStrings.Count > 0)
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["SqlConn"]))
                {
                    sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["SqlConn"]].ConnectionString);
                }
                else
                {
                    sqlConnection = new SqlConnection(ConfigurationManager.AppSettings["SqlConn"]);
                }
            }
            return sqlConnection;
        }
    }
}
