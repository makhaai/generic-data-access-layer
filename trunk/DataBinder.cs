﻿/*
 * Generic Database Binder class
 * Author: Raju Melveetilpurayil
 * Date  : 28-11-2014
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Collections;

namespace Generic.Data
{
    public static class DataBinder
    {
        private static T eval<T, U>(T t, U u)
            where T : class
            where U : DataSet
        {
            try
            {
                Type type = t.GetType();

                if (!u.IsNullOrEmpty())
                {
                    DataTable dt = u.Tables[0];
                    if (!dt.IsNullOrEmpty())
                    {
                        for (var rows = 0; rows < dt.Rows.Count; rows++)
                        {
                            for (var column = 0; column < dt.Columns.Count; column++)
                            {
                                string columnName = dt.Columns[column].ColumnName;
                                var dbValue = dt.Rows[rows][column];

                                if (dbValue != DBNull.Value)
                                {
                                    PropertyInfo prop = type.GetProperty(columnName);
                                    if (prop != null)
                                    {
                                        type.GetProperty(columnName).SetValue(t, dbValue, null);
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return t;
        }

        /// <summary>
        /// Build Generic List 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="t"></param>
        /// <param name="u"></param>
        /// <returns></returns>
        private static List<T> evalList<T, U>(T t, U u)
            where T : class,new()
            where U : DataSet
        {
            List<T> items = new List<T>();
            Type type = t.GetType();
            try
            {
                if (!u.IsNullOrEmpty())
                {
                    DataTable dt = u.Tables[0];
                    if (!dt.IsNullOrEmpty())
                    {
                        for (var rows = 0; rows < dt.Rows.Count; rows++)
                        {
                            T newtype = new T();

                            for (var column = 0; column < dt.Columns.Count; column++)
                            {
                                string columnName = dt.Columns[column].ColumnName;
                                var dbValue = dt.Rows[rows][column];

                                if (dbValue != DBNull.Value)
                                {
                                    PropertyInfo prop = type.GetProperty(columnName);
                                    if (prop != null)
                                    {
                                        type.GetProperty(columnName).SetValue(newtype, dbValue, null);
                                    }
                                }
                            }
                            items.Add(newtype);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }


        public static int Update<T>(T t, string storedProcedueName) where T : class
        {
            int reuslt = new SqlHelper().Update(t, storedProcedueName);
            return reuslt;
        }
        public static int Update(string storedProcedueName, object sqlParams)
        {
            int reuslt = new SqlHelper().Update(storedProcedueName, sqlParams);
            return reuslt;
        }

        /// <summary>
        /// Return Single Item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="stroedProcedre"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public static T Eval<T>(T t, string stroedProcedre, object sqlParams)
            where T : class, new()
        {
            DataSet ds = new SqlHelper().Read(stroedProcedre, sqlParams);
            t = eval(t, ds) as T;
            return t;
        }

        /// <summary>
        /// Return List item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="stroedProcedre"></param>
        /// <param name="sqlParams"></param>
        /// <returns></returns>
        public static List<T> EvalList<T>(T t, string stroedProcedre, object sqlParams=null)
            where T : class, new()
        {

            DataSet ds = new SqlHelper().Read(stroedProcedre, sqlParams);
            List<T> list = new List<T>();
            list.AddRange(evalList(t, ds));
            return list;
        }

        /// <summary>
        /// Send calss Not accepts Generic List
        /// </summary>
        /// <typeparam name="T">Class Only</typeparam>
        /// <param name="t">type</param>
        /// <returns>List<SqlParameter></returns>
        internal static List<SqlParameter> EvalSqlParameter<T>(T t) where T : class
        {
            List<SqlParameter> paramList = new List<SqlParameter>();
            try
            {
                /*PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    var value = property.GetValue(t, null);
                    paramList.Add(new SqlParameter("@" + property.Name, value));
                }*/
                PropertyInfo[] properties = typeof(T).GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    object[] cAttributes = property.GetCustomAttributes(true);
                    if (cAttributes.Length > 0)
                    {
                        foreach (object cAttr in cAttributes)
                        {
                            // Need to Ignore this
                            if (cAttr.GetType() == new IgnoreOnUpdate().GetType())
                            {
                            }
                            else
                            {
                                var value = property.GetValue(t, null);
                                paramList.Add(new SqlParameter("@" + property.Name, value));
                            }
                        }
                    }
                    else
                    {
                        var value = property.GetValue(t, null);
                        paramList.Add(new SqlParameter("@" + property.Name, value));
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return paramList;
        }
    }
}
