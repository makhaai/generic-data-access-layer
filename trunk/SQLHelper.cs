﻿/*
 * Generic Database Binder class
 * Author: Raju Melveetilpurayil
 * Date  : 28-11-2014
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Generic.Data
{
    internal class SqlHelper
    {
        private DataAccess dataAccess;
        public SqlHelper()
        {
            dataAccess = new DataAccess();
        }



        /// <summary>
        /// Update DataBase without parameter, It return Updated or Inserted item Id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="storedProcedueName"></param>
        /// <returns></returns>
        public int Update<T>(T t, string storedProcedueName) where T:class
        {
            int result = -1;
            try
            {
                List<SqlParameter> sqlParams = DataBinder.EvalSqlParameter<T>(t);
                result = dataAccess.UpdateDatabase(storedProcedueName, sqlParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public int Update(string storedProcedueName, object sqlParams)
        {
            int result = -1;
            try
            {

                List<SqlParameter> _sqlParams = null;
                if (sqlParams != null)
                {
                    _sqlParams = new List<SqlParameter>();

                    var source = sqlParams.GetType().GetProperties();

                    for (int index = 0; index < source.Length; index++)
                    {
                        _sqlParams.Add(new SqlParameter("@" + source[index].Name.ToString(),
                                                      source[index].GetValue(sqlParams,null)));
                    }
                }

                result = dataAccess.UpdateDatabase(storedProcedueName, _sqlParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public DataSet Read(string storedProcedueName,object sqlParams)
        {
            DataSet ds = new DataSet();
            try
            {

                List<SqlParameter> _sqlParams = null;
                if (sqlParams != null)
                {
                    _sqlParams = new List<SqlParameter>();

                    var source = sqlParams.GetType().GetProperties();
                    for (int index = 0; index < source.Length; index++)
                    {
                        _sqlParams.Add(new SqlParameter("@" + source[index].Name.ToString(),
                                                     source[index].GetValue(sqlParams, null)));
                    }
                }

                ds = dataAccess.ReadDatabase(storedProcedueName, _sqlParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
    }
}
