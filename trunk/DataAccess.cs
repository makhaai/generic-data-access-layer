﻿/*
 *  * 
 * Author:Raju Melveetilpurayil
 * Version: 1.0.0.1
 * Update: Added Finllay for Try to close sql connections
 *         And handled exceptions
 * 
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Data.Common;

namespace Generic.Data
{
    /// <summary>
    /// Data Layer
    /// </summary>
    internal class DataAccess
    {
        /// <summary>
        /// Insert and Update database
        /// </summary>
        /// <param name="sDbNodeName">Config element name for Stored Procedure </param>
        /// <param name="sqlParams">Sql Parameters </param>
        /// <returns></returns>
        public int UpdateDatabase(string storedProcedure, List<SqlParameter> sqlParams)
        {
            int iResult = -1;
            try
            {
                using (SqlConnection sqlConn = Provider.GetConnection())
                {
                    sqlConn.Open();

                    SqlCommand sqlCmd = new SqlCommand();
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.CommandText = storedProcedure;
                    sqlCmd.Connection = sqlConn;
                    if (sqlParams != null)
                    {
                        foreach (SqlParameter sqlParam in sqlParams)
                        {
                            sqlCmd.Parameters.Add(sqlParam);
                        }
                    }

                    DbParameter returnValue;
                    returnValue = sqlCmd.CreateParameter();
                    returnValue.Direction = ParameterDirection.ReturnValue;
                    sqlCmd.Parameters.Add(returnValue);

                    sqlCmd.ExecuteNonQuery();
                    iResult = Convert.ToInt32(returnValue.Value);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return iResult;
        }
        /// <summary>
        /// Read database
        /// </summary>
        /// <param name="sDbNodeName">Config element name for Stored Procedure </param>
        /// <param name="sqlParams">Sql Parameters </param>
        /// <param name="sErrorMsg">Error Messages</param>
        /// <param name="iResult">Return Items</param>
        /// <returns></returns>
        public DataSet ReadDatabase(string storedProcedure, List<SqlParameter> sqlParams)
        {
            DataSet ds = null;
            
            try
            {
                using (SqlConnection sqlConn = Provider.GetConnection())
                {
                    if (sqlConn != null)
                    {
                        sqlConn.Open();

                        SqlCommand sqlCmd = new SqlCommand();
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.CommandText = storedProcedure;
                        sqlCmd.Connection = sqlConn;

                        if (sqlParams != null)
                        {
                            foreach (SqlParameter sqlParam in sqlParams)
                            {
                                sqlCmd.Parameters.Add(sqlParam);
                            }
                        }
                        
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        ds = new DataSet();
                        da.Fill(ds);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
    }
}