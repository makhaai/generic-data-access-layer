﻿/*
 * Extensions
 * Author: Raju Melveetilpurayil
 * Date  : 28-11-2014
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Generic.Data
{
   public static class Extensions
    {
        /// <summary>
        /// Null or Empty check for DataSet
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this DataSet ds)
        {
            bool success = true;

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    success = false;
                }
            }
            return success;
        }
        /// <summary>
        /// Null or Empty check for DataTable 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this DataTable dt)
        {
            bool success = true;

            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    success = false;
                }
            }
            return success;
        }
    }
}
