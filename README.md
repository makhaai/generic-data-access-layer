# README #

Generic Data layer for accessing database. 
Database tables and Model classes want to be the same.

### What is this repository for? ###


* Reduce coding
* Easy to implement
* Current version 1.0.0.6

### How to use ###


```
#!c#
using Generic.Data;

public class CommentManager 
{
	public CommentManager()
	{
	}

	public int Update(Comments t)
	{
		int result = -1;
		result = DataBinder.Update<Comments>(t, "storedProcedureForAddAndUpdateComments");
		return result;
	}

	public List<Comments> GetAll()
	{
		List<Comments> cmd = DataBinder.EvalList(new Comments(), "storedProcedureForGetAllComments", null);
		return cmd;
	}

	public Comments GetSingle(int Id)
	{
		Comments comment = new Comments();
		comment = DataBinder.Eval(new Comments(), "storedProcedureForGetSingleById", new { CommentId = Id });
		return comment;
	}
}
```